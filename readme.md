## An app using Parliament API to display MPs profiles
This project uses [Parliament API](http://data.parliament.uk/membersdataplatform/memberquery.aspx) which provides information about the MPs

It is build using plain javascript and [tailwind](https://tailwindcss.com/). The reason to use the former is to show that it can be build without a js framework. The reason for tailwind was because I never used it before and wanted to give it a go. 😇 - it might make more sense to use in a rather large application though. Also used a webpack to serve static files, bundle js and css and transpiling ES6.

## Instructions
### Run application
Clone this repo then access the folder using your terminal and run `npm i && npm run serve` should  open automatically in the browser [http://localhost:8080/](http://localhost:8080/).

### To do next:
Time for this sort of projects is always scarce with two daughters at home but test and update this with Vuejs would be the next steps.
