// Global config
const GLOBAL_CONFIG = {
  fetch: {
    headers: {
      // This is necessary otherwise it's an XML format
      Accept: 'application/json',
    },
  },
  API: {
    baseURL: 'http://data.parliament.uk/membersdataplatform/services/',
    MPQuery: 'mnis/members/query/',
    MPImgQuery: 'images/MemberPhoto/',
    partyImgQuery: 'images/PartyLowRes/',
    mpEligible: '|House=Commons|IsEligible=true',
    mpWithPost: '|holdsgovernmentpost=true',
    mpInfo(mp) {
      return `id=${mp}/Committees%7CConstituencies%7CStaff`;
    },
  },
};

// Build API url for all purposes
const urlBuild = (urlBase, options) => `${urlBase + options}/`;

// Fecth error handler
const fetchError = (err) => {
  const listContainer = document.querySelector('.js-mp-list');

  listContainer.innerHTML = `<p class="p-2"><strong>Error loading MPs</strong>. More info: ${err}</p>`;
};

// Main fetch
const apiFetch = async (apiURL) => {
  try {
    const response = await fetch(apiURL, GLOBAL_CONFIG.fetch);
    return await response.json();
  } catch (err) {
    fetchError(err);
  }
};

const formatDate = date => ((typeof date === 'string') ? new Date(date).toLocaleDateString('en-GB') : 'current');

const closeDialog = (e) => {
  // Only allow keyup when it's ESC key
  if ((e.type === 'keyup' && e.key.toLowerCase() === 'escape') || (e.type === 'click')) {
    const dialogVeil = document.querySelector('.js-mp-panel');
    const mpPanel = document.querySelector('.js-dialog-panel');

    dialogVeil.classList.remove('mp-dialog-veil--enable');
    mpPanel.classList.remove('mp-dialog-panel--opened');
    dialogVeil.setAttribute('aria-hidden', true);
  }
};

const mpInfoPanel = (mp) => {
  const mpInfoData = mp.Members.Member;
  const dialogVeil = document.querySelector('.js-mp-panel');
  const mpPanel = document.querySelector('.js-dialog-panel');
  const mpInfo = document.querySelector('.js-mp-info');
  const closeDialogBtn = document.querySelector('.js-btn-close-panel');
  const committeesList = mpInfoData.Committees && mpInfoData.Committees.Committee.length > 0 ? mpInfoData.Committees.Committee.map(committee => `<li>From ${formatDate(committee.StartDate)} to ${formatDate(committee.EndDate)}: <strong>${committee.Name}</strong></li>`).join('') : '<li>Not part of any committees</li>';
  const constituenciesList = mpInfoData.Constituencies && mpInfoData.Constituencies.Constituency.length > 0 ? mpInfoData.Constituencies.Constituency.map(constituency => `<li>From ${formatDate(constituency.StartDate)} to ${formatDate(constituency.EndDate)}: <strong>${constituency.Name}</strong></li>`).join('') : '<li>Not part of any constituencies</li>';
  const staffList = mpInfoData.Staffing && mpInfoData.Staffing.Staff.length > 0 ? mpInfoData.Staffing.Staff.map(staff => `<li><strong>${staff.Title} ${staff.Glass} ${staff.Maree}</strong> - ${staff.Note}</li>`).join('') : '<li>No staff available</li>';

  dialogVeil.classList.add('mp-dialog-veil--enable');
  mpPanel.classList.add('mp-dialog-panel--opened');
  dialogVeil.setAttribute('aria-hidden', false);

  mpInfo.innerHTML = `
    <h2 class="flex pb-5">
      <img src="${GLOBAL_CONFIG.API.baseURL + GLOBAL_CONFIG.API.MPImgQuery + mpInfoData['@Member_Id']}/" class="mr-2" alt="${mpInfoData.Party['#text']}"/>
      <div class="flex items-start flex-col">
        <span class="text-xl font-bold mb-2">${mpInfoData.DisplayAs}</span>
        <span><img src="${GLOBAL_CONFIG.API.baseURL + GLOBAL_CONFIG.API.partyImgQuery + mpInfoData.Party['@Id']}/" class="w-32" alt="${mpInfoData.Party['#text']}"/></span>
      </div>
    </h2>
    <h3 class="mb-2 text-lg border-b-2 font-bold">Committees</h3>
    <ul class="pb-4">${committeesList}</ul>
    <h3 class="mb-2 text-lg border-b-2 font-bold">Constituencies</h3>
    <ul class="pb-4">${constituenciesList}</ul>
    <h3 class="mb-2 text-lg border-b-2 font-bold">Staff</h3>
    <ul class="pb-4">${staffList}</ul>
  `;

  // Avoid binding event listener to button multiple times
  if (!closeDialogBtn.dataset.hasEvent) {
    closeDialogBtn.addEventListener('click', closeDialog);
    document.addEventListener('keyup', closeDialog);
    closeDialogBtn.dataset.hasEvent = true;
  }
};

const mpInfo = (e) => {
  const { mpId } = e.target.dataset;
  const apiQuery = GLOBAL_CONFIG.API.baseURL + GLOBAL_CONFIG.API.MPQuery;
  const apiURL = urlBuild(apiQuery, GLOBAL_CONFIG.API.mpInfo(mpId));

  apiFetch(apiURL).then(data => mpInfoPanel(data));
};

const populteList = (mps) => {
  const members = mps.Members && mps.Members.Member;
  const listContainer = document.querySelector('.js-mp-list');

  // Checks if there's any mp
  if (mps.Members.Member.length > 0) {
    const mpList = members.map(mp => `<li class="p-2 border-b border-gray-200">
      <button class="transition duration-300 ease-in-out bg-transparent border-b border-purple-700 hover:bg-yellow-500 text-purple-700 font-semibold hover:text-black py-1 px-2 border border-blue-500 hover:border-transparent rounded js-more-info" data-mp-id="${mp['@Member_Id']}">
        ${mp.DisplayAs}
        (${mp.Party['#text']})
      </button>
    </li>`).join('');

    listContainer.innerHTML = `<ul>${mpList}</ul>`;

    // Bind event after buttons are added
    const mpMoreInfo = [...document.querySelectorAll('.js-more-info')];

    mpMoreInfo.map(button => button.addEventListener('click', mpInfo));
  } else {
    listContainer.innerHTML = '<p class="p-2">No records found</p>';
  }
};

const filterMPsWithPost = () => {
  const queryURL = GLOBAL_CONFIG.API.baseURL + GLOBAL_CONFIG.API.MPQuery;
  const requestOptions = GLOBAL_CONFIG.API.mpEligible + GLOBAL_CONFIG.API.mpWithPost;
  const apiURL = urlBuild(queryURL, requestOptions);

  apiFetch(apiURL).then(data => populteList(data));
};

export const initUI = () => {
  const queryURL = GLOBAL_CONFIG.API.baseURL + GLOBAL_CONFIG.API.MPQuery;
  const apiURL = urlBuild(queryURL, GLOBAL_CONFIG.API.mpEligible);
  const filterBtn = document.querySelector('.js-filter');
  const refreshBtn = document.querySelector('.js-refresh');

  filterBtn.addEventListener('click', filterMPsWithPost);
  refreshBtn.addEventListener('click', () => {
    apiFetch(apiURL).then(data => populteList(data));
  });

  // Init app
  apiFetch(apiURL).then(data => populteList(data));
};
